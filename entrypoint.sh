#!/bin/sh

VOLUME=${VOLUME:-"volume"}
ALLOW=${ALLOW:-192.168.0.0/16 172.16.0.0/12}

# Check if group id already exists
GROUPINFO=$(getent group ${DLP_GID} | cut -d: -f1)
if [ "$GROUPINFO" = "" ]; then
	addgroup --gid ${DLP_GID} dlp
else
	groupmod -n dlp "${GROUPINFO}"
fi

useradd -u ${DLP_UID} -g ${DLP_GID} -M dlp

cat <<EOF > /etc/rsyncd.conf
uid = ${DLP_UID}
gid = ${DLP_GID}
use chroot = yes
pid file = /var/run/rsyncd.pid
log file = /dev/stdout
[${VOLUME}]
    hosts deny = *
    hosts allow = ${ALLOW}
    read only = false
    path = /volume
    comment = ${VOLUME}
EOF

exec rsync --no-detach --daemon --config /etc/rsyncd.conf "$@"
